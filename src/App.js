import './App.css';

// import components
 import AppNavbar from './components/AppNavbar.js';
 import Footer from './components/Footer.js';
 import ItemView from './components/ItemView.js';
 import ProfileView from './components/ProfileView.js';

// import pages
 import ErrorPage from './pages/ErrorPage.js';
 import Home from './pages/Home.js';

 import About from './pages/About.js';
 import Menu from './pages/Menu.js';
 import Location from './pages/Location.js';
 import Delivery from './pages/Delivery.js';
 import Career from './pages/Career.js';
 import Contact from './pages/Contact.js';

 import Register from './pages/Register.js';
 import Login from './pages/Login.js';
 import Logout from './pages/Logout.js';
 import Dashboard from './pages/Dashboard.js';
 import Orders from './pages/Orders.js';


// import modules
 import { useState , useEffect } from 'react';
 import { BrowserRouter as Router , Routes , Route } from 'react-router-dom';

// import UserProvider
 import { UserProvider } from './UserContext.js'

function App() {

  // const [user, setUser] = useState(localStorage.getItem("username"));
  const [user, setUser] = useState(null);

  useEffect(()=>{
    // console.log(user);
  }, [user]);

  const unSetUser = () => {
    localStorage.clear()
    // setUser(localStorage.getItem("username"));
   }

  useEffect(()=>{
    fetch(`${process.env.REACT_APP_LOCAL}/user/profile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    }).then(result => result.json()
    ).then(data => {
      // console.log(data); // pag wala nakalog-in then return is null
      if(localStorage.getItem('token') !== null){
          console.log(data);
          setUser({
            id: data._id,
            username: data.username,
            isAdmin: data.isAdmin
          })
      } else{
          setUser(null);
      }
    })
  },[]);

  return (
    <UserProvider value = { { user , setUser , unSetUser } }>
      <Router>
        <AppNavbar />
        <Routes>
          <Route path="*" element = {<ErrorPage/>} />
          <Route path="/" element = {<Home/>} />

          <Route path="/about" element = {<About/>} />
          <Route path="/menu" element = {<Menu/>} />
          <Route path="/location" element = {<Location/>} />
          <Route path="/delivery" element = {<Delivery/>} />
          <Route path="/career" element = {<Career/>} />
          <Route path="/contact" element = {<Contact/>} />

          <Route path="/register" element = {<Register/>} />
          <Route path="/login" element = {<Login/>} />
          <Route path="/logout" element = {<Logout/>} />
          <Route path="/dashboard" element = {<Dashboard/>} />
          <Route path="/orders" element = {<Orders/>} />
          <Route path = '/product/:productId' element = {<ItemView/>} />
          <Route path = '/user/:userId' element = {<ProfileView/>} />
        </Routes>
        <Footer />
      </Router>
    </UserProvider>
  );
  
}

export default App;


import {Fragment, useEffect, useState, useContext} from 'react';
import {Container, Row, Col , Button , Modal , Form } from 'react-bootstrap';
import UserContext from '../UserContext.js';

import ItemCard from "./ItemCard.js"
import Swal from 'sweetalert2';

export default function AllProducts () {

    const [products, setProducts] = useState([]);
    const {user} = useContext(UserContext);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_LOCAL}/product`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(result => result.json())
        .then(data => {
            // console.log(data)
            setProducts(data.map(product => {
                // console.log(product._id)
                return(
                    <ItemCard key = {product._id} itemProp = {product}/>
                    )
            }))
        })
    }, []);

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [category, setCategory] = useState('');
    // console.log(name)

    function createItem(){

          fetch(`${process.env.REACT_APP_LOCAL}/product/createProduct`, {
            method: 'POST',
            headers: {
              'Content-Type' : 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
              name: name,
              description: description,
              price: price,
              category: category
            })
          }).then(result => result.json()
          ).then(data => {
            // console.log(data);
            if(data){
              Swal.fire({
                title: "Product Added!",
                icon: "success"
              })
              setShow(false);
              setName('');
              setDescription('');
              setPrice('');
              setCategory('');

            } else{
              Swal.fire({
                title: "Unsuccessful!",
                icon: "error",
                text: "Something went wrong!"
              })
            }   
          })
    }

    
    return(
        <Fragment>
        <Container>
          <Row>
            <Col className = "text-center">
            <h3 className = "mt-3"> ALL PRODUCTS</h3>
            <Button id="add-item-btn" variant="secondary" className="mx-3" onClick={handleShow}> + Add Product</Button>
                    <Modal show={show} onHide={handleClose}>
                        <Modal.Header closeButton>
                          <Modal.Title>Create Item</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                          <Form>
                            <Form.Group className="mb-3">
                            <Form.Label className="fw-bold">Product Name:</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder= "Product Name"
                                value= {name}
                                onChange = {event => setName(event.target.value)}
                                autoFocus
                            />
                          </Form.Group>
                          <Form.Group className="mb-3">
                            <Form.Label className="fw-bold">Description:</Form.Label>
                            <Form.Control
                                as="textarea" rows={3}
                                placeholder= "Description"
                                value= {description}
                                onChange = {event => setDescription(event.target.value)}
                                autoFocus
                            />
                        </Form.Group>
                            <Form.Group className="mb-3">
                            <Form.Label className="fw-bold">Price:</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder= "Price"
                                value = {price}
                                onChange = {event => setPrice(event.target.value)}
                                autoFocus
                            />
                          </Form.Group>
                          <Form.Group className="mb-3">
                            <Form.Label className="fw-bold">Category:</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder= "Category"
                                value = {category}
                                onChange = {event => setCategory(event.target.value)}
                                autoFocus
                            />
                          </Form.Group>
                          </Form>
                        </Modal.Body>
                        <Modal.Footer>
                          <Button variant="dark" onClick={handleClose}>Close</Button>
                          <Button variant="primary" onClick={() => createItem()}>Save Changes</Button>
                        </Modal.Footer>
                      </Modal>
            </Col>
            {products}
          </Row>
        </Container>

        </Fragment>
        )
};


import {Fragment, useEffect, useState, useContext} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import UserContext from '../UserContext.js';

import ItemCard from "./ItemCard.js"

export default function AllProducts () {

    const [available, setAvailable] = useState([]);
    const {user} = useContext(UserContext);

    useEffect(() => {

        fetch(`${process.env.REACT_APP_LOCAL}/product/available`)
        .then(result => result.json())
        .then(data => {
            setAvailable(data.map(active => {
                return(
                    <ItemCard key = {active._id} itemProp = {active}/>
                    )
            }))
        })
    }, []);

    return(
        <Fragment>
        <Container fluid className="mt-5 mb-5">
          <Row>
            <Col>
            <h3 className = "text-center mt-3"> PRODUCTS</h3>
            {available}
            </Col>
          </Row>
        </Container>

        </Fragment>
        )
};

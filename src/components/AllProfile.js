
import {Fragment, useEffect, useState, useContext} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import UserContext from '../UserContext.js';

import ProfileCard from "./ProfileCard.js"

export default function AllProfile () {

    const [profile, setProfile] = useState([]);
    const {user} = useContext(UserContext);

    useEffect(() => {

        fetch(`${process.env.REACT_APP_LOCAL}/user`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(result => result.json())
        .then(data => {
            // console.log(data)
            setProfile(data.map(prof => {
                // console.log(prof._id)
                return(
                    <ProfileCard key = {prof._id} profileProp = {prof}/>
                    )
            }))
        })
    }, []);

    return(
        <Fragment>
        <Container fluid className="mt-5 mb-5">
          <Row>
            <Col>
            <h3 className = "text-center mt-3"> PROFILES</h3>
            {profile}
            </Col>
          </Row>
        </Container>

        </Fragment>
        )
};

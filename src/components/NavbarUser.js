// imports
 import { useState , useEffect , useContext } from 'react'; 
 import { Container , Navbar } from 'react-bootstrap';
 import { Link } from 'react-router-dom';

 import UserContext from '../UserContext.js'

// export
export default function NavbarUser() {

  const [username, setUsername] = useState(null);
  const [id, setId] = useState(null);
  const {user, setUser} = useContext(UserContext);

  // useEffect(() => {
  //   setUser(user)
  //   if(user !== null){      
  //   setUsername(user.username);
  //   }
  // }, [user, setUser]);

  useEffect(()=>{
    // console.log(productId);

    fetch(`${process.env.REACT_APP_LOCAL}/user/profile`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }     
    })
    .then(result => result.json())
    .then(data => {
      // console.log(data);
      setUsername(data.username);
      setId(data._id);
    })
  },[])


  return (
    <Navbar id="navbar-user" bg="dark" variant="dark" expand="lg">
      {
        user ?
        <Container fluid>
            <Navbar.Text>Welcome to ElectriciTea! <Link to = "/menu" id="elecTEAfied">Get #elecTEAfied </Link> </Navbar.Text>
            {
              user.isAdmin ?
                <Navbar.Text className="justify-content-end"> Welcome, <Link to = {`/user/${id}`}> {username} </Link>! Go to <Link to = "/dashboard"> Dashboard </Link>.. | <Link to = "/logout" className="text-danger">Logout</Link></Navbar.Text>
              :
                <Navbar.Text className="justify-content-end">Signed in as: <Link to = {`/user/${id}`}> {username} </Link> | <Link to = "/logout" className="text-danger">Logout</Link></Navbar.Text>
            }
            
        </Container>
        :
        <Container fluid>
            <Navbar.Text>Welcome to ElectriciTea!</Navbar.Text>
            <Navbar.Text className="justify-content-end"> <Link to = "/login"> Login </Link> | <Link to = "/register"> Register </Link> </Navbar.Text>
        </Container>
      }    
      
    </Navbar>
  );
}


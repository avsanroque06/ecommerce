// imports
 import { Fragment , useContext ,useState } from 'react';
 import { Row , Col , Card , Button , Form , Modal } from 'react-bootstrap';
 import { Link , useParams , useNavigate } from 'react-router-dom';
 
import Swal from 'sweetalert2';
 import UserContext from '../UserContext.js'

export default function ItemCard({itemProp}){

	const { _id , name , description , price , isActive , category } = itemProp;
	const {user} = useContext(UserContext);
	const { productId } = useParams();
	const navigate = useNavigate();

	const archive = (id) => {
		fetch(`${process.env.REACT_APP_LOCAL}/product/archive/${_id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}			
		})
		.then(result => result.json())
		.then(data => {
			if(data.isActive === false){
				Swal.fire({
					title: "Item successfully Archived!",
					icon: "success",
					text: "Item is now unavailable!"
				})
			} else {
				Swal.fire({
					title: "Item successfully Unarchived!",
					icon: "success",
					text: "Item is now available!"
				})
			}
			navigate(`/dashboard`);
		})
		// .catch(error => console.log(error))
	}

	const order = (id) => {
		fetch(`${process.env.REACT_APP_LOCAL}/order/checkout/${_id}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}			
		})
		.then(result => result.json())
		.then(data => {
			// console.log(data);
			Swal.fire({
				title: "Order Created!",
				icon: "success"
			})
		})
	}

	//update item
	  const [show, setShow] = useState(false);
	  const handleClose = () => setShow(false);
	  const handleShow = () => setShow(true);

	  const [name1, setName1] = useState(name);
  	  const [description1, setDescription1] = useState(description);
  	  const [price1, setPrice1] = useState(price);

	  const updateItem = (id) => {

	   	  fetch(`${process.env.REACT_APP_LOCAL}/product/update/${_id}`, {
	   		method: 'PUT',
	   		headers: {
	   		  'Content-Type' : 'application/json',
	  			Authorization: `Bearer ${localStorage.getItem('token')}`
	   		},
	   		body: JSON.stringify({
	   		  name: name1,
	   		  description: description1,
	   		  price: price1
	   		})
	   	  }).then(result => result.json()
	   	  ).then(data => {
	   	  	// console.log(data);
	   	  	if(data){
	   	  	  Swal.fire({
	   	  		title: "Item Updated!",
	   	  		icon: "success"
	   	  	  })
	   	  	  setShow(false);
	   	  	  navigate(`/dashboard`);
	   	  	} else{
	   	  	  Swal.fire({
	   	  		title: "Error!",
	   	  		icon: "error",
	   	  		text: "Something went wrong!"
	   	  	  })
	   	  	}	
	   	  })
	  	}

	

	return(
	<Row className = "mt-5">	
	  <Col>
      	<Card className = "card col-10 mx-auto">
		  <Card.Body>
			<Card.Title>{name}</Card.Title>
			<Card.Subtitle>Description:</Card.Subtitle>
			<Card.Text>{description}</Card.Text>
			<Card.Text>{category}</Card.Text>
			<Card.Text>Php {price}</Card.Text>
			{
				user.isAdmin ?
				<Fragment>
				<Button variant="info" className="m-2" as = {Link} to = {`/product/${_id}`}>View Item</Button>
				<Button variant="warning" className="m-2" onClick={handleShow}>Update Item</Button>
				  	<Modal show={show} onHide={handleClose}>
				        <Modal.Header closeButton>
				          <Modal.Title>Update Item</Modal.Title>
				        </Modal.Header>
				        <Modal.Body>
				          <Form>
				            <Form.Group className="mb-3">
					        <Form.Label className="fw-bold">Product Name:</Form.Label>
					        <Form.Control
					            type="text"
					            placeholder= {name}
					            onChange = {event => setName1(event.target.value)}
					            autoFocus
					        />
					      </Form.Group>
					      <Form.Group className="mb-3">
					        <Form.Label className="fw-bold">Description:</Form.Label>
					        <Form.Control
					            as="textarea" rows={3}
					            placeholder= {description}
					            onChange = {event => setDescription1(event.target.value)}
					            autoFocus
					        />
					    </Form.Group>
				            <Form.Group className="mb-3">
					        <Form.Label className="fw-bold">Price:</Form.Label>
					        <Form.Control
					            type="text"
					            placeholder= {price}
					            onChange = {event => setPrice1(event.target.value)}
					            autoFocus
					        />
					      </Form.Group>
				          </Form>
				        </Modal.Body>
				        <Modal.Footer>
				          <Button variant="dark" onClick={handleClose}>Close</Button>
				          <Button variant="primary" onClick={() => updateItem(productId)}>Save Changes</Button>
				        </Modal.Footer>
				      </Modal>
				{
					isActive ?
					<Button variant="danger" className="m-2" onClick={() => archive(productId)}>Archive</Button>
					:
					<Button variant="success" className="m-2" onClick={() => archive(productId)}>Unarchive</Button>
				}
				</Fragment>
				:
				<Fragment>
				<Button variant="info" className="m-2" as = {Link} to = {`/product/${_id}`}>View Item</Button>
				<Button variant="primary" className="m-2" onClick={() => order(productId)}>Order</Button>
				</Fragment>
			}
		  </Card.Body>
		</Card>
	  </Col>
	</Row>
  )
}

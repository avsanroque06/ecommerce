import { Fragment , useState , useEffect , useContext } from 'react';
import { Card , Button , Modal , Form } from 'react-bootstrap';
import { useParams , useNavigate , Link } from 'react-router-dom';
import UserContext from '../UserContext.js'

import Swal from 'sweetalert2';

export default function ProfileView(){
	//constsssssss
	 const [username, setUsername] = useState('');
	 const [firstName, setFirstName] = useState('');
	 const [lastName, setLastName] = useState('');
	 const [mobileNo, setMobileNo] = useState('');
	 const [email, setEmail] = useState('');
	 const [isAdmin, setIsAdmin] = useState('');

	 const navigate = useNavigate();
	 const {user} = useContext(UserContext);
	 const { userId } = useParams();

	 const [role, setRole] = useState("User")
	 // console.log(isAdmin)
	 useEffect(()=>{
		if(isAdmin === true){
			setRole("Admin")
		} else{
			setRole("User")
		}
	 } ,[isAdmin])

	useEffect(()=>{
		// console.log(productId);

		fetch(`${process.env.REACT_APP_LOCAL}/user/${userId}`)
		.then(result => result.json())
		.then(data => {
			console.log(data);
			setUsername(data.username);
			setFirstName(data.firstName);
			setLastName(data.lastName);
			setMobileNo(data.mobileNo);
			setEmail(data.email);
			setIsAdmin(data.isAdmin);
		})

	}, [userId])

	const setAdmin = (id) => {
		fetch(`${process.env.REACT_APP_LOCAL}/user/setAdmin/${id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}			
		})
		.then(result => result.json())
		.then(data => {
			// console.log(data)
			if(data.isAdmin === true){
				Swal.fire({
					title: "Success",
					icon: "success",
					text: `${data.username} has successfully been made to be an Admin!` 
				})
			} else {
				Swal.fire({
					title: "Success",
					icon: "success",
					text: `${data.username}'s Admin Role has been revoked!`
				})
			}
			navigate(`/dashboard`);
		})
		// .catch(error => console.log(error))
	}

	const resetPass = (id) => {
		fetch(`${process.env.REACT_APP_LOCAL}/user/pwChange/${id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}			
		})
		.then(result => result.json())
		.then(data => {
			// console.log(data)
			if(data){
				Swal.fire({
					title: `Succesfully reset ${data.username}'s password`,
					icon: "success",
					text: "Tell them it's their username :>" 
				})
			} else {
				Swal.fire({
					title: "Error",
					icon: "error",
					text: `idk wth happened!`
				})
			}
			navigate(`/dashboard`);
		})
		// .catch(error => console.log(error))
	}

	//modal
	 const [show, setShow] = useState(false);
	 const handleClose = () => setShow(false);
	 const handleShow = () => setShow(true);

	 // change password
	  const [currentPassword, setCurrentPassword] = useState('');
	  const [newPassword, setNewPassword] = useState('');

	  function changePass(event){
	 	event.preventDefault();

 	  fetch(`${process.env.REACT_APP_LOCAL}/user/pwChange`, {
 		method: 'PUT',
 		headers: {
 		  'Content-Type' : 'application/json',
			Authorization: `Bearer ${localStorage.getItem('token')}`
 		},
 		body: JSON.stringify({
 		  currentPassword: currentPassword,
 		  newPassword: newPassword
 		})
 	  }).then(result => result.json()
 	  ).then(data => {
 	  	// console.log(data);
 	  	if(data){
 	  	  Swal.fire({
 	  		title: "Password Changed!",
 	  		icon: "success"
 	  	  })
 	  	  setShow(false);
 	  	  setNewPassword('')
 	  	  setCurrentPassword('')
 	  	} else{
 	  	  Swal.fire({
 	  		title: "Error!",
 	  		icon: "error",
 	  		text: "You may have entered a wrong password!"
 	  	  })
 	  	}	
 	  })
	  }
	   //capslock
	   const [isCapsLockOn, setIsCapsLockOn] = useState(false);
	   const checkCapsLock = (event) => {
	     if (event.getModifierState('CapsLock')) {
	       setIsCapsLockOn(true);
	     } else {
	       setIsCapsLockOn(false);
	    }
	   }

	 //update user
	   const [showUpdate, setShowUpdate] = useState(false);
	   const updateClose = () => setShowUpdate(false);
	   const updateShow = () => setShowUpdate(true);

	   const [username1, setUsername1] = useState(username);
	   const [firstName1, setFirstName1] = useState(firstName);
	   const [lastName1, setLastName1] = useState(lastName);
	   const [mobileNo1, setMobileNo1] = useState(mobileNo);
	   const [email1, setEmail1] = useState(email);
	   	  function update(event){
	   	 	event.preventDefault();

	    	  fetch(`${process.env.REACT_APP_LOCAL}/user/update`, {
	    		method: 'PUT',
	    		headers: {
	    		  'Content-Type' : 'application/json',
	   			Authorization: `Bearer ${localStorage.getItem('token')}`
	    		},
	    		body: JSON.stringify({
	    		  username: username1,
	    		  firstName: firstName1,
	    		  lastName: lastName1,
	    		  email: email1,
	    		  mobileNo: mobileNo1
	    		})
	    	  }).then(result => result.json()
	    	  ).then(data => {
	    	  	// console.log(data);
	    	  	if(data){
	    	  	  Swal.fire({
	    	  		title: "Profile Changed!",
	    	  		icon: "success"
	    	  	  })
	    	  	  setShowUpdate(false);
	    	  	} else{
	    	  	  Swal.fire({
	    	  		title: "Error!",
	    	  		icon: "error",
	    	  		text: "Something went wrong! username/email already exist!"
	    	  	  })
	    	  	}	
	    	  })
	   	  }
	 


	return(
	  <Fragment>
        <Card className = "view-card col-10 mx-auto">
		  <Card.Body>
			<Card.Title className="text-primary">{username}</Card.Title>
			<Card.Subtitle>Role:</Card.Subtitle>
			<Card.Text>{role}</Card.Text>
			<Card.Subtitle>Name:</Card.Subtitle>
			<Card.Text>{firstName} {lastName}</Card.Text> 
			<Card.Subtitle>Email:</Card.Subtitle>
			<Card.Text>{email}</Card.Text>
			<Card.Subtitle>Mobile Number:</Card.Subtitle>
			<Card.Text>{mobileNo}</Card.Text>
			{
				user.isAdmin ?
				<Fragment>
				{
					!isAdmin ?
					<Button variant="warning" className="m-2" onClick={() => setAdmin(userId)}>Set as Admin</Button>
					:
					<Button variant="danger" className="m-2" onClick={() => setAdmin(userId)}>Revoke Admin Role</Button>
				}
				<Button variant="primary" className="m-2" onClick={() => resetPass(userId)}>Reset Password</Button>
				<Button variant="dark" className="m-2" as = {Link} to = '/dashboard'>View Profiles</Button>
				</Fragment>
				:
				<Fragment>
				<Button variant="success" className="m-2" as = {Link} to = '/orders'>View Orders</Button>
				<Button variant="warning" className="m-2" onClick={updateShow}> Update Profile </Button>		
				<Button variant="primary" className="m-2" onClick={handleShow}> Change Password </Button>
				</Fragment>
			}
		  </Card.Body>
		</Card>
				<Modal show={show} onHide={handleClose}>
				  <Modal.Header closeButton>
				    <Modal.Title>Change Password</Modal.Title>
				  </Modal.Header>
				  <Modal.Body>
				    <Form >
				      <Form.Group className="mb-3">
				        <Form.Label className="fw-bold">Current Password</Form.Label>
				        <Form.Control
				            type="password"
				            placeholder="Current Password"
				            value = {currentPassword}
				            onKeyUp = { checkCapsLock }
				            onChange = {event => setCurrentPassword(event.target.value)}
				            required
				            autoFocus
				        />
				      </Form.Group>
				      <Form.Group className="mb-3">
				            <Form.Label className="fw-bold">New Password</Form.Label>
				        <Form.Control
				            type="password"
				            placeholder="New Password"
				            value = {newPassword}
				            onKeyUp = { checkCapsLock }
				            onChange = {event => setNewPassword(event.target.value)}
				            required
				            autoFocus
				        />
				          {isCapsLockOn && (
				           <p className = "text-danger">Caps Lock is ON</p>
				          )}
				      </Form.Group>
				    </Form>
				  </Modal.Body>
				  <Modal.Footer>
				    <Button variant="secondary" onClick={handleClose}> Close </Button>
				    <Button variant="primary" onClick={event => changePass(event)}> Save Changes </Button>
				  </Modal.Footer>
				 </Modal>

				<Modal show={showUpdate} onHide={updateClose}>
				  <Modal.Header closeButton>
				    <Modal.Title>Update Profile</Modal.Title>
				  </Modal.Header>
				  <Modal.Body>
				    <Form >
				      <Form.Group className="mb-3">
				        <Form.Label className="fw-bold">Username:</Form.Label>
				        <Form.Control
				            type="text"
				            placeholder= {username}
				            onChange = {event => setUsername1(event.target.value)}
				            autoFocus
				        />
				      </Form.Group>
				      <Form.Group className="mb-3">
				            <Form.Label className="fw-bold">First Name:</Form.Label>
				        <Form.Control
				            type="text"
				            placeholder={firstName}
				            onChange = {event => setFirstName1(event.target.value)}
				            autoFocus
				        />
				      </Form.Group>
				      <Form.Group className="mb-3">
				            <Form.Label className="fw-bold">Last Name:</Form.Label>
				        <Form.Control
				            type="text"
				            placeholder={lastName}
				            onChange = {event => setLastName1(event.target.value)}
				            autoFocus
				        />
				      </Form.Group>
				      <Form.Group className="mb-3">
				            <Form.Label className="fw-bold">Email:</Form.Label>
				        <Form.Control
				            type="email"
				            placeholder={email}
				            onChange = {event => setEmail1(event.target.value)}
				            autoFocus
				        />
				      </Form.Group>
				      <Form.Group className="mb-3">
				            <Form.Label className="fw-bold">Mobile Number:</Form.Label>
				        <Form.Control
				            type="text"
				            placeholder={mobileNo}
				            onChange = {event => setMobileNo1(event.target.value)}
				            autoFocus
				        />
				      </Form.Group>
				    </Form>
				  </Modal.Body>
				  <Modal.Footer>
				    <Button variant="secondary" onClick={updateClose}> Close </Button>
				    <Button variant="primary" onClick={event => update(event)}> Save Changes </Button>
				  </Modal.Footer>
				 </Modal>

    </Fragment>
	)


}
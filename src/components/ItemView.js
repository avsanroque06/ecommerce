import { Fragment , useState , useEffect , useContext } from 'react';
import { Card , Button ,Modal , Form } from 'react-bootstrap';
import { useParams , useNavigate , Link } from 'react-router-dom';
import UserContext from '../UserContext.js'

import Swal from 'sweetalert2';

export default function ItemView(){
    //constsssssss
	 const [name, setName] = useState('');
	 const [description, setDescription] = useState('');
	 const [price, setPrice] = useState('');
	 const [isActive, setIsActive] = useState('');

	 const navigate = useNavigate();
	 const {user} = useContext(UserContext);
	 const { productId } = useParams();

	const archive = (id) => {
		fetch(`${process.env.REACT_APP_LOCAL}/product/archive/${id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}			
		})
		.then(result => result.json())
		.then(data => {
			if(data.isActive === false){
				Swal.fire({
					title: "Item successfully Archived!",
					icon: "success",
					text: "Item is now unavailable!"
				})
			} else {
				Swal.fire({
					title: "Item successfully Unarchived!",
					icon: "success",
					text: "Item is now available!"
				})
			}
			navigate(`/dashboard`);
		})
		// .catch(error => console.log(error))
	}

	const order = (id) => {
		fetch(`${process.env.REACT_APP_LOCAL}/order/checkout/${id}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}			
		})
		.then(result => result.json())
		.then(data => {
			// console.log(data)
			Swal.fire({
				title: "Order Created!",
				icon: "success"
			})
		})
	}

	//update item
	  const [show, setShow] = useState(false);
	  const handleClose = () => setShow(false);
	  const handleShow = () => setShow(true);

	  const [name1, setName1] = useState(name);
  	  const [description1, setDescription1] = useState(description);
  	  const [price1, setPrice1] = useState(price);

	  const updateItem = (id) => {

	   	  fetch(`${process.env.REACT_APP_LOCAL}/product/update/${id}`, {
	   		method: 'PUT',
	   		headers: {
	   		  'Content-Type' : 'application/json',
	  			Authorization: `Bearer ${localStorage.getItem('token')}`
	   		},
	   		body: JSON.stringify({
	   		  name: name1,
	   		  description: description1,
	   		  price: price1
	   		})
	   	  }).then(result => result.json()
	   	  ).then(data => {
	   	  	// console.log(data);
	   	  	if(data){
	   	  	  Swal.fire({
	   	  		title: "Item Updated!",
	   	  		icon: "success"
	   	  	  })
	   	  	  setShow(false);
	   	  	  navigate(`/dashboard`);
	   	  	} else{
	   	  	  Swal.fire({
	   	  		title: "Error!",
	   	  		icon: "error",
	   	  		text: "Something went wrong!"
	   	  	  })
	   	  	}	
	   	  })
	  	}

	useEffect(()=>{
		// console.log(productId);

		fetch(`${process.env.REACT_APP_LOCAL}/product/${productId}`)
		.then(result => result.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setIsActive(data.isActive);
		})

	}, [productId])

	return(
	  <Fragment>
      <Card className = "view-card mx-auto m-5 p-3">
  		  <Card.Body>
  			<Card.Title>{name}</Card.Title>	
  			<Card.Text>{description}</Card.Text>
  			<Card.Text>Php {price}</Card.Text>
  			{
  				user.isAdmin ?
  				<Fragment>
  				{
  					isActive ?
  					<Button variant="danger" className="m-3" onClick={() => archive(productId)}>Archive</Button>
  					:
  					<Button variant="success" className="m-3" onClick={() => archive(productId)}>Unarchive</Button>
  				}
  				<Button variant="warning" className="m-3" onClick={handleShow}>Update Item</Button>
  				  	<Modal show={show} onHide={handleClose}>
  				        <Modal.Header closeButton>
  				          <Modal.Title>Update Item</Modal.Title>
  				        </Modal.Header>
  				        <Modal.Body>
  				          <Form>
  				            <Form.Group className="mb-3">
  					        <Form.Label className="fw-bold">Product Name:</Form.Label>
  					        <Form.Control
  					            type="text"
  					            placeholder= {name}
  					            onChange = {event => setName1(event.target.value)}
  					            autoFocus
  					        />
  					      </Form.Group>
  					      <Form.Group className="mb-3">
  					        <Form.Label className="fw-bold">Description:</Form.Label>
  					        <Form.Control
  					            as="textarea" rows={3}
  					            placeholder= {description}
  					            onChange = {event => setDescription1(event.target.value)}
  					            autoFocus
  					        />
  					    </Form.Group>
  				            <Form.Group className="mb-3">
  					        <Form.Label className="fw-bold">Price:</Form.Label>
  					        <Form.Control
  					            type="text"
  					            placeholder= {price}
  					            onChange = {event => setPrice1(event.target.value)}
  					            autoFocus
  					        />
  					      </Form.Group>
  				          </Form>
  				        </Modal.Body>
  				        <Modal.Footer>
  				          <Button variant="dark" onClick={handleClose}>Close</Button>
  				          <Button variant="primary" onClick={() => updateItem(productId)}>Save Changes</Button>
  				        </Modal.Footer>
  				      </Modal>
  				<Button variant="secondary" className="m-3" as = {Link} to = '/dashboard'>Back to List</Button>
  				</Fragment>
  				:
  				<Fragment>
  				<Button variant="primary" onClick={() => order(productId)}>Order</Button>
  				<Button variant="secondary" className="mx-3" as = {Link} to = '/menu'>Back to List</Button>
  				</Fragment>
  			}
  		  </Card.Body>
  		</Card>
  	</Fragment>
	)
}
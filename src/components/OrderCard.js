// imports
 // import { useContext } from 'react';
 import { Row , Col , Card } from 'react-bootstrap';
 // import { useNavigate , useParams } from 'react-router-dom';
 
// import Swal from 'sweetalert2';
 // import UserContext from '../UserContext.js'

// export default function ItemCard({itemProp}){
export default function OrderCard({orderProp}){

	const { productName , userId , productId , quantity , totalAmount } = orderProp;
	// const {user} = useContext(UserContext);
	// const navigate = useNavigate();

	return(
	<Row className = "mt-5">	
	  <Col>
      	<Card className = "card col-10 mx-auto">
		  <Card.Body>
			<Card.Title>{productName}</Card.Title>
			<Card.Subtitle>UserId:</Card.Subtitle>
			<Card.Text>{userId}</Card.Text>
			<Card.Subtitle>ProductId:</Card.Subtitle>
			<Card.Text>{productId}</Card.Text> 
			<Card.Subtitle>Quantity:</Card.Subtitle>
			<Card.Text>{quantity}</Card.Text>
			<Card.Subtitle>Total Amount:</Card.Subtitle>
			<Card.Text>{totalAmount}</Card.Text>
		  </Card.Body>
		</Card>
	  </Col>
	</Row>
  )
}

//imports
 import { Fragment , useState , useEffect, useContext } from 'react';
 import { Button , Form , Container , Row , Col } from 'react-bootstrap';
 import { Navigate , useNavigate , Link } from "react-router-dom";

 import UserContext from '../UserContext.js';

 import Swal from 'sweetalert2';

//export
export default function Register() {

  	// useStates
  	 //user input
   	 const [username, setUsername] = useState('');
   	 const [firstName, setFirstName] = useState('');
   	 const [lastName, setLastName] = useState('');
   	 const [email, setEmail] = useState('');
   	 const [mobileNo, setMobileNo] = useState('');
   	 const [password, setPassword] = useState('');
   	 const [confirmPassword, setConfirmPassword] = useState('');
   	 //button
	 const [isActive, setIsActive] = useState(false);

   	// UserContext and navigate
   	 const { user , setUser } = useContext(UserContext);
  	 const navigate = useNavigate();

  	// useEffects
  	 useEffect(() => {
  	   if(
  		 username !== "" && 
  		 firstName !== "" && 
  		 lastName !== "" && 
  		 email !== "" && 
  		 mobileNo !== "" && 
  		 password !== "" && 
  		 confirmPassword !== "" && 
  		 password === confirmPassword
  		 ){
  			setIsActive(true);
  	   } else{
  		 setIsActive(false);
  	   }
  	 }, [username,firstName,lastName,email,mobileNo,password,confirmPassword]);

  	//REGISTER function
  	 function register(event){
		event.preventDefault();

		// alert("Registered na!");
		// localStorage.setItem('username', username);
		// setUser(localStorage.getItem('username'));
		// setEmail('');
		// setPassword('');
		// setConfirmPassword('');

	  //fetch request from API (register)
	  fetch(`${process.env.REACT_APP_LOCAL}/user/register`, {
		method: 'POST',
		headers: {
		  'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
		  username: username,
		  firstName: firstName,
		  lastName: lastName,
		  email: email,
		  mobileNo: mobileNo,
		  password: password,
		  confirmPassword: confirmPassword
		})
	  }).then(result => result.json()
	  ).then(data => {
		// console.log(data);
		if(data){
			localStorage.setItem('token', data.auth);
            retrieveUserDetails(localStorage.getItem('token'));
		  Swal.fire({
			title: "Registration Success!",
			icon: "success",
			text: "Thank you for registering! GET elecTEAfied!!"
		  })

		} else{
		  Swal.fire({
			title: "Registration Unsuccessful!",
			icon: "error",
			text: "Username / Email might have already been registered.."
		  })
		}	
	  })//fetch

	  const retrieveUserDetails = (token) => {
	    fetch(`${process.env.REACT_APP_LOCAL}/user/profile`, {
	        headers:{
	            Authorization: `Bearer ${token}`
	        }
	    })
	    .then(result => result.json())
	    .then(data => {
	        // console.log(data)
	        setUser({
	            id: data._id,
	            username: data.username,
	            isAdmin: data.isAdmin
	        })

	        if(data.isAdmin){
	          navigate("/dashboard")
	        } else {
	          navigate("/")
	        }

	    })
	  }//retrieve
	 }

	//capslock on
	 const [isCapsLockOn, setIsCapsLockOn] = useState(false);
	 const checkCapsLock = (event) => {
	   if (event.getModifierState('CapsLock')) {
	     setIsCapsLockOn(true);
	   } else {
	     setIsCapsLockOn(false);
	   }
	 }

	// // username taken
	//  const [isUsernameTaken, setIsUsernameTaken] = useState(false)
	//  const checkUsername ()=> {
	//  	fet
	//  }

	//return
	return(
	 user ?
	 <Navigate to = "*" />
	 :
	 <Container fluid id="register-container">
      <Row className = "m-5">
      	<Col className = "col-md-8 col-lg-10 mx-auto bg-light p-3">
      	  <Fragment>
      		<h1 className = "text-center">REGISTER</h1>
      		<Form className = "mt-5" onSubmit = {event => register(event)}>

      		  <Form.Group className="mb-3" id="registerUsername">
      			<Form.Label className = "fw-bold">Username</Form.Label>
      			<Form.Control 
      			  type="text" 
      			  placeholder="Username" 
      			  value = {username}
      			  onChange = {event => setUsername(event.target.value)}
      			  required
      			  />
      		  </Form.Group>

      		  <Form.Group className="mb-3" id="registerFirstName">
      			<Form.Label className = "fw-bold">First Name</Form.Label>
      			<Form.Control 
      			  type="text" 
      			  placeholder="First Name" 
      			  value = {firstName}
      			  onChange = {event => setFirstName(event.target.value)}
      			  required
      			  />
      		  </Form.Group>

      		  <Form.Group className="mb-3" id="registerLastName">
      			<Form.Label className = "fw-bold">Last Name</Form.Label>
      			<Form.Control 
      			  type="text" 
      			  placeholder="Last Name" 
      			  value = {lastName}
      			  onChange = {event => setLastName(event.target.value)}
      			  required
      			  />
      		  </Form.Group>

      		  <Form.Group className="mb-3" id="registerMobileNo">
      			<Form.Label className = "fw-bold">Mobile Number</Form.Label>
      			<Form.Control 
      			  type="text" 
      			  placeholder="(+63) 9123456789"
      			  value = {mobileNo}
      			  onChange = {event => setMobileNo(event.target.value)}
      			  required
      			  />
      		  </Form.Group>

      		  <Form.Group className="mb-3" id="registerEmail">
      			<Form.Label className = "fw-bold">Email Address</Form.Label>
      			<Form.Control 
      			  type="email" 
      			  placeholder="YourEmail@mail.com" 
      			  value = {email}
      			  onChange = {event => setEmail(event.target.value)}
      			  required
      			  />
      			</Form.Group>

      		  <Form.Group className="mb-3" id="registerPassword">
      			<Form.Label className = "fw-bold">Password</Form.Label>
      			<Form.Control 
      			  onKeyUp = { checkCapsLock }
      			  type="password" 
      			  placeholder="Password" 
      			  value = {password}
      			  onChange = {event => setPassword(event.target.value)}
      			  required
      			  />
      			  <Form.Text className="text-muted fw-lighter">Case Sensitive</Form.Text>
      		  </Form.Group>

      		  <Form.Group className="mb-3" id="registerConfirmPassword">
      			<Form.Label className = "fw-bold">Confirm Password</Form.Label>
      			<Form.Control 
      			  type="password" 
      			  placeholder="Confirm Password" 
      			  value = {confirmPassword}
      			  onKeyUp = { checkCapsLock }
      			  onChange = {event => setConfirmPassword(event.target.value)}
      			  required
      			  />
      			 {isCapsLockOn && (
      			  <p className = "text-danger">Caps Lock is ON</p>
      			 )}
      		  </Form.Group>

      		  {    
      			isActive ? 
      			<Button variant="warning" type="submit">Submit</Button>
      			:
      			<Button variant="dark" type="submit" disabled>Submit</Button>
      		  }
      		  <Form.Label className="mx-2"> Already have an account? Login <Link to = "/login">here</Link>.</Form.Label>

      		</Form>

      	  </Fragment>
      	</Col>
       </Row>
     </Container> 
	)

}
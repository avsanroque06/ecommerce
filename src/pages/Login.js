//imports
 import { Fragment , useState , useEffect } from 'react'; 
 import { useContext } from 'react'; 
 import { Form , Button , Container , Row , Col } from 'react-bootstrap';
 import { Navigate , useNavigate } from 'react-router-dom';
 import { Link } from 'react-router-dom';

 import UserContext from '../UserContext.js';

// sweetalert
 import Swal from 'sweetalert2';

//export
export default function Login() {

  const [ username , setUsername ] = useState('');
  const [ password , setPassword ] = useState('');

  const navigate = useNavigate();

  const { user, setUser } = useContext(UserContext);
  // const [user, setUser] = useState(localStorage.getItem('username'));

  // button
  const [ isActive , setIsActive ] = useState(false); 
  // console.log(user);

  useEffect(() => {
      if(username !== "" && password !== ""){
          setIsActive(true);
      } else{
          setIsActive(false);
      }
  }, [username, password]);

  // LOGIN function
  function login(event){
      event.preventDefault();

      // localStorage.setItem("username", username)
      // alert("Logged in na.")
      // setUsername('');
      // setPassword('');

      fetch(`${process.env.REACT_APP_LOCAL}/user/login`, {
          method: 'POST',
          headers: {
              'Content-Type' : 'application/json'
          },
          body: JSON.stringify({
              username: username,
              password: password
          })
      }).then(result => result.json()
      ).then(data => {
          // console.log(data);
          if(data === false){
            Swal.fire({
              title: "Authentication Failed",
              icon: "error",
              text: "Please try again :>"
            })
          } else{ 
              localStorage.setItem('token', data.auth);
              retrieveUserDetails(localStorage.getItem('token'));

              //if successfully logged in
              Swal.fire({
                title: "Authentication Success",
                icon: "success",
                text: "Get #elecTEAfied!"
              })
            }
      })//fetch

      const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_LOCAL}/user/profile`, {
            headers:{
                Authorization: `Bearer ${token}`
            }
        })
        .then(result => result.json())
        .then(data => {
            // console.log(data)
            setUser({
                id: data._id,
                username: data.username,
                isAdmin: data.isAdmin
            })

            if(data.isAdmin){
              navigate("/dashboard")
            } else {
              navigate("/")
            }

        })
      }//retrieve

  }//login

  // if CapsLock is ON
  const [isCapsLockOn, setIsCapsLockOn] = useState(false);
  const checkCapsLock = (event) => {
    if (event.getModifierState('CapsLock')) {
      setIsCapsLockOn(true);
    } else {
      setIsCapsLockOn(false);
    }
  }

  return(
   user ?
   <Navigate to = "*"/>
   :
   <Container fluid id="login-container">
    <Row className = "m-5">
      <Col className = "col-md-8 col-lg-6 mx-auto bg-light p-3">
        <Fragment>
          <h1 className = "text-center">LOGIN</h1>

          <Form className = "mt-5" onSubmit = {event => login(event)}>
            <Form.Group className="mb-3" id="loginUsername">
              <Form.Label className = "fw-bold">Username</Form.Label>
              <Form.Control 
                type="text" 
                placeholder="Enter Username"
                value = {username}
                onChange = {event => setUsername(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" id="loginPassword">
              <Form.Label className = "fw-bold">Password</Form.Label>
              <Form.Control
                type="password" 
                placeholder="Enter Password" 
                value = {password}
                onKeyUp = { checkCapsLock }
                onChange = {event => setPassword(event.target.value)}
                required
              />
              <Form.Text className="text-muted fw-lighter">Case Sensitive</Form.Text>
              {isCapsLockOn && (
                <p className = "text-danger">CapsLock is ON</p>
              )}
            </Form.Group>
              {
                isActive ?
                  <Button variant="warning" type="submit">Submit</Button>
                :
                  <Button variant="dark" type="submit" disabled>Submit</Button>                
              }
            <Form.Label className="mx-2"> Dont't have an account yet? Create one now! Register <Link to = "/register">here</Link>.</Form.Label>
                </Form>
        </Fragment>
      </Col>
    </Row>
   </Container>
    )

}
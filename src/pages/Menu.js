import { Container } from 'react-bootstrap';
import { Fragment , useContext  } from 'react';
 // import { Navigate } from 'react-router-dom';

import ActiveProducts from '../components/ActiveProducts.js';
import NavbarUser from '../components/NavbarUser.js';
import UserContext from '../UserContext.js';

export default function Menu () {

	const {user} = useContext(UserContext);

	return(
			<Fragment>
			  <NavbarUser />
			  <Container fluid className="mt-5 mb-5">
			  <ActiveProducts />
			  </Container>
			</Fragment>

	)

}
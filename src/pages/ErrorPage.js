
import { Fragment } from 'react';
import { Container , Row , Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import NavbarUser from '../components/NavbarUser.js';

export default function ErrorPage() {
  
  return (
  	<Fragment>
  	<NavbarUser/>
  	  <Container fluid className='text-center p-2' style={{ backgroundColor: 'rgba(255, 255, 51, 0.50)' }}>
  	  	<Row id="error-page">
  	  		<Col>
	  	  		<img id="error-img" src="https://i.ibb.co/NY5MyJy/crying-mt.jpg" alt="crying-mt"/>
	  	  	</Col>
	  	  		<h1 className = "fw-bold">There's NO MILK TEA here!!!!</h1>
						<p className = "mt-3">Click <Link to = "/">here</Link> to go back to Home Page.</p>
						<p>Click <Link to = "/register">here</Link> to Register an Account.</p>
				  	<p>If you already have an account, click <Link to = "/login">here</Link> to login.</p>
				</Row>
  	  </Container>
  	  
	  </Fragment>
  )

}